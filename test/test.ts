import { expect, tap } from '@push.rocks/tapbundle';
import * as typedrequest from '@api.global/typedrequest';
import * as typedrequestInterfaces from '@api.global/typedrequest-interfaces';

import * as typedsocket from '../ts/index.js';

interface IRequest_Client_Server
  extends typedrequestInterfaces.implementsTR<
    typedrequestInterfaces.ITypedRequest,
    IRequest_Client_Server
  > {
  method: 'sayhi';
  request: {
    greeting: string;
  };
  response: {
    answer: string;
  };
}

let testTypedSocketServer: typedsocket.TypedSocket;
let testTypedSocketClient: typedsocket.TypedSocket;

const testTypedRouter = new typedrequest.TypedRouter();

tap.test('should add some handlers', async () => {
  testTypedRouter.addTypedHandler<IRequest_Client_Server>(
    new typedrequest.TypedHandler('sayhi', async (requestData) => {
      return {
        answer: `ok, got it : ${requestData.greeting}`,
      };
    })
  );
});

tap.test('should create Server and Client', async (tools) => {
  testTypedSocketServer = await typedsocket.TypedSocket.createServer(testTypedRouter);
  testTypedSocketClient = await typedsocket.TypedSocket.createClient(
    testTypedRouter,
    'http://localhost:3000'
  );
  console.log('test: waiting 5 seconds');
  await tools.delayFor(5000);
  await testTypedSocketServer.stop();

  // lets create another server
  testTypedSocketServer = await typedsocket.TypedSocket.createServer(testTypedRouter);

  // lets see if auto reconnect works
  console.log('test: waiting 21 seconds for reconnect');
  await tools.delayFor(21000);
});

tap.test('should process messages from both sides', async () => {
  const myServerSideTypedRequest =
    testTypedSocketServer.createTypedRequest<IRequest_Client_Server>('sayhi');
  const myClientSideTypedRequest =
    testTypedSocketClient.createTypedRequest<IRequest_Client_Server>('sayhi');
  const response = await myClientSideTypedRequest.fire({
    greeting: 'that is a greeting from the client',
  });
  console.log(response);
  const response2 = await myServerSideTypedRequest.fire({
    greeting: 'that is a greeting from the server',
  });
  console.log(response2);
});

tap.test('should disconnect', async (tools) => {
  await testTypedSocketClient.stop();
  await testTypedSocketServer.stop();
  tools.delayFor(1000).then(() => process.exit(0));
});

tap.start();
