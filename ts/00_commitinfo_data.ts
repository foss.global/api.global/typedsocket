/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@api.global/typedsocket',
  version: '3.0.0',
  description: 'a typedrequest extension supporting websockets'
}
